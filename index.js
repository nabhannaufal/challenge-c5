const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser');
const user = require('./data/user.json');

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(express.static('public'))
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('chapter-3')
})

app.get('/chapter-4', (req, res) => {
    res.render('chapter-4')
})

app.get('/login', (req, res) => {
    res.render('login')
})

app.get('/user', (req, res) => {
    res.status(200).json(user)
  })
  
  app.post('/login', (req, res) => {
    const { username, password } = req.body;
    const cekUsername = user.filter((item) => item.username === username);
    let cekPassword = false;
    if (cekUsername.length > 0) {
      if (password === cekUsername[0].password) {
        cekPassword = true;
      }
    }
  
    if (cekPassword) {
        res.status(200).render('login-berhasil');
    }
  
    res.status(400).render('login-gagal');
  })

app.listen(port, () => {
    console.log(`server udah jalan bos! coba cek localhost:${port}`)
})